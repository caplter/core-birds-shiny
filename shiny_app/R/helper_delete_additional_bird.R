#' @title helper: delete an existing additional bird observation
#'
#' @description Function to delete a additional bird observation from the
#' additional_bird_observations table for a given survey.

delete_additional_bird <- function(observation_to_delete) {

  base_query <- "
  DELETE FROM core_birds.additional_bird_observations
  WHERE additional_bird_observations.id = ?observation_id
  ;
  "

  parameterized_query <- DBI::sqlInterpolate(
    DBI::ANSI(),
    base_query,
    observation_id = observation_to_delete
  )

  run_interpolated_execution(parameterized_query)
  print(parameterized_query)

}
