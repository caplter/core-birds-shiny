#' @title core birds observers from the database
#'
#' @description Makes available the list of ACTIVE observers in the database

query_observers <- function() {

  base_query <- "
  SELECT
    observers.id,
    observers.observer,
    observers.active
  FROM core_birds.observers
  ORDER BY observers.id DESC
  -- WHERE observers.active = TRUE
  ;
  "

  observers <- DBI::dbGetQuery(
    conn      = this_pool,
    statement = base_query
  )

  return(observers)

}

observers_all <- query_observers()

observers <- observers_all |>
  dplyr::filter(active == TRUE)
