#' @title helper: add a new bird record
#'
#' @description Function to add a new bird record to the birds table for a given
#' survey.

insert_new_bird <- function(
  survey_id,
  taxon,
  distance,
  count,
  seen,
  heard,
  direction,
  notes,
  query_type,
  bird_obs_id
  ) {

  survey_id <- as.integer(survey_id)
  taxon_id  <- glue::glue_sql("{bird_taxa[bird_taxa$compound_name == taxon,]$id*}")
  seen      <- ifelse(seen  == 1, "true", "false")
  heard     <- ifelse(heard == 1, "true", "false")
  direction <- ifelse(direction == "", "NA", direction)
  notes     <- ifelse(notes == "", "NA", notes)
  notes     <- gsub("[\r\n]", "; ", notes)

  if (query_type == "insert") {

    base_query <- "
    INSERT INTO core_birds.bird_observations
    (
      survey_id,
      bird_taxon_id,
      distance,
      bird_count,
      seen,
      heard,
      direction,
      notes
    )
    VALUES
    (
      ?argument_survey_id,
      ?argument_taxon_id,
      ?argument_distance,
      ?argument_count,
      ?argument_seen,
      ?argument_heard,
      NULLIF(?argument_direction, 'NA')::text,
      NULLIF(?argument_notes, 'NA')::text
      )
    ;
    "

    parameterized_query <- DBI::sqlInterpolate(
      DBI::ANSI(),
      base_query,
      argument_survey_id = survey_id,
      argument_taxon_id  = taxon_id,
      argument_distance  = distance,
      argument_count     = count,
      argument_seen      = seen,
      argument_heard     = heard,
      argument_direction = direction,
      argument_notes     = notes
    )

    run_interpolated_execution(parameterized_query)
    # print(parameterized_query)

  } else {

    parameterized_query <- glue::glue_sql("
      UPDATE core_birds.bird_observations
      SET
        bird_taxon_id = { taxon_id },
        distance = { distance },
        bird_count = { count },
        notes = NULLIF({ notes }, 'NA')::text,
        seen = { seen },
        heard = { heard },
        direction = { direction }
      WHERE
        id = { bird_obs_id }
      ;
      ",
      .con = DBI::ANSI()
    )

    run_interpolated_execution(parameterized_query)
    # print(parameterized_query)

  }

}
