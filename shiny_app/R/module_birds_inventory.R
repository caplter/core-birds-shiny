#' @title module: inventory of birds for a given survey
#'
#' @description The birds_inventory module facilitates viewing, adding new,
#' editing, and deleting core bird records.

# birds inventory main UI ------------------------------------------------------

birds_inventoryUI <- function(id) {

  ns <- shiny::NS(id)

  tagList(

    shiny::fluidRow(
      id = "row_reference_survey_details",
      shiny::verbatimTextOutput(ns("survey_site_details"))
      ), # close row

    shiny::fluidRow(
      shiny::column(
        width = 10
        ),
      shiny::column(
        width = 2,
        shiny::actionButton(
          inputId = ns("add_bird"),
          label   = "add bird",
          class   = "btn-success",
          style   = "color: #fff; margin-bottom: 2px;",
          icon    = icon('plus'),
          width   = "100%"
        )
      )
      ),

    shiny::fluidRow(
      id = "row_birds_inventory",

      shiny::column(
        id    = "col_birds_inventory",
        width = 12,
        DT::DTOutput(ns("birds_inventory_view"))
      ) # close column

      ), # close row

    # js file and function within file respectively
    tags$script(src = "birds_inventory_module.js"),
    tags$script(paste0("birds_inventory_module_js('", ns(''), "')"))

  ) # close tagList

} # close UI


# birds inventory main function ------------------------------------------------

birds_inventory <- function(input, output, session, survey_id) {

  # setup

  # added to facilitate renderUIs
  ns <- session$ns


  # query survey details for reference
  survey_details_reactive <- reactive({

    survey_details_queried <- query_survey(
      survey = survey_id()
    )

    return(survey_details_queried)

  })

  output$survey_site_details <- shiny::renderText({

    paste0(
      "survey id: ", survey_id(), " | ",
      "site: ", survey_details_reactive()[["site_code"]], " | ",
      "date: ", survey_details_reactive()[["survey_date"]], " | ",
      "observer: ", survey_details_reactive()[["observer"]]
    )

  })


  # query birds data
  birds_inventory_reactive <- reactive({

    listener_watch("update_birds")

    birds_inventory_queried <- query_birds(
      survey = survey_id()
    )

    birds_inventory_queried <- dplyr::bind_rows(
      empty_bird_frame |> dplyr::filter(!is.na(id)),
      birds_inventory_queried
    )

    if (nrow(birds_inventory_queried) == 0) {

      birds_inventory_queried <- NULL

    } else {

      birds_inventory_queried <- birds_inventory_queried |>
      dplyr::select(-survey_id) |>
      dplyr::mutate(
        seen = as.character(seen),
        seen = dplyr::case_when(
          seen == TRUE  ~ "x",
          seen == FALSE ~ "",
          TRUE ~ seen
          ),
        heard = as.character(heard),
        heard = dplyr::case_when(
          heard == TRUE  ~ "x",
          heard == FALSE ~ "",
          TRUE ~ heard
        )
      )

      actions <- purrr::map_chr(birds_inventory_queried$id, function(id_) {
        paste0(
          '<div class="btn-group" style="width: 120px;" role="group" aria-label="Basic example">
          <button class="btn btn-primary btn-sm edit_btn" data-toggle="tooltip" data-placement="top" title="Edit" id = ', id_, ' style="margin: 0"><i class="fa fa-pencil-square-o"></i></button>
          <button class="btn btn-danger btn-sm delete_btn" data-toggle="tooltip" data-placement="top" title="Delete" id = ', id_, ' style="margin-left: 5px;"><i class="fa fa-trash-o"></i></button>
          </div>'
        )
      }
      )

      birds_inventory_queried <- cbind(
        birds_inventory_queried,
        tibble::tibble("actions" = actions)
      )

    }

    return(birds_inventory_queried)

  })


  # render table of data
  output$birds_inventory_view <- DT::renderDT({

    birds_inventory_reactive()

  },
  class      = "cell-border stripe",
  plugins    = c("ellipsis"),
  escape     = FALSE,
  selection  = "none",
  rownames   = FALSE,
  options    = list(
    scrollX       = TRUE,
    bFilter       = 0,
    bLengthChange = FALSE,
    bPaginate     = FALSE,
    bSort         = TRUE,
    autoWidth     = FALSE,
    columnDefs    = list(
      list(
        targets = c(11),
        render  = JS("$.fn.dataTable.render.ellipsis( 8 )")
        ),
      list(
        targets   = c(8, 9, 12),
        className = "dt-center"
      ),
      list(
        targets   = c(12),
        orderable = FALSE
      )
    )
  )
  ) # close table output


  # module: edit bird

  this_observation_to_edit <- shiny::eventReactive(input$bird_id_to_edit, {

    this_bird_observation <- query_bird_observation(bird_obs_id = input$bird_id_to_edit)

    return(this_bird_observation)

  })

  module_bird_new(
    id            = "edit_bird",
    modal_title   = "edit_bird",
    bird_to_edit  = this_observation_to_edit,
    survey_id     = survey_id,
    modal_trigger = reactive({input$bird_id_to_edit})
  )


  # module: add bird

  module_bird_new(
    id            = "add_bird",
    modal_title   = "add bird",
    bird_to_edit  = function() NULL,
    survey_id     = survey_id,
    modal_trigger = reactive({input$add_bird})
  )


  # delete bird observation

  shiny::observeEvent(input$bird_id_to_delete, {

    tryCatch({

      delete_bird(observation_to_delete = input$bird_id_to_delete)

      listener_trigger("update_birds")

    }, warning = function(warn) {

      shiny::showNotification(
        ui          = paste("warning: ", warn),
        duration    = NULL,
        closeButton = TRUE,
        type        = "warning"
      )

    }, error = function(err) {

      shiny::showNotification(
        ui          = paste("error: ", err),
        duration    = NULL,
        closeButton = TRUE,
        type        = "error"
      )

    }) # close tryCatch

  }) # close delete bird


  # debugging: module level -------------------------------------------------

  # observe(print({ birds_inventory_reactive() |> dplyr::select(-actions) }))


} # close module
