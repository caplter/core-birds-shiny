#' @title core birds taxonomic details from the database
#'
#' @description Makes available bird taxonomy details

query_bird_taxa <- function() {

  base_query <- "
  SELECT
    id,
    code,
    common_name,
    CONCAT(code, ' - ', common_name) AS compound_name
  FROM core_birds.bird_taxa
  ORDER BY code ASC
  ;
  "

  bird_taxa <- DBI::dbGetQuery(
    conn      = this_pool,
    statement = base_query
  )

  return(bird_taxa)

}

bird_taxa <- query_bird_taxa()
